package com.twuc.webApp.controllers;

import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.GameGuess;
import com.twuc.webApp.model.GameGuessResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GameController {

    @PostMapping("/games")
    public ResponseEntity createGame() {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/games/"+ Game.create().getId()).build();
    }

    @GetMapping("/games/{id}")
    public Game getGameById(@PathVariable Integer id) {
        Game game = Game.find(id);
        if (game == null) {
            throw new IllegalArgumentException("game not found by "+id);
        }
        return game;
    }

    @PatchMapping("/games/{id}")
    public GameGuessResult guessById(@PathVariable Integer id, @RequestBody GameGuess g) {
        Game game = Game.find(id);
        if (game == null) {
            throw new IllegalArgumentException("game not found by "+id);
        }
        return game.guess(g);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    public ResponseEntity<String> handle() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
