package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class Game {
    private static AtomicInteger incrementId = new AtomicInteger( 1 );
    private static Map<Integer, Game> games = new ConcurrentHashMap<>();

    public static Game create() {
        Game game = new Game(incrementId.getAndAdd(1));
        games.put(game.getId(), game);
        return game;
    }

    public static Game find(Integer id) {
        return games.get(id);
    }

    private Integer id;
    private String answer;

    Game() {}

    Game(Integer id) {
        this.id = id;
        int[] ints = ThreadLocalRandom.current().ints(0, 9).distinct().limit(4).toArray();
        this.answer = Arrays.stream(ints)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(""));
    }

    public Integer getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public GameGuessResult guess(GameGuess g) {
        String[] guess = g.getAnswer().split("");
        String[] right = getAnswer().split("");

        int includes = 0;
        int matches = 0;
        for (int i = 0; i < guess.length; i++) {
            if (right[i].equals(guess[i])) {
                matches++;
            } else if (g.getAnswer().contains(right[i])) {
                includes++;
            }
        }

        return new GameGuessResult(matches+"A"+includes+"B", g.getAnswer().equals(answer));
    }
}
