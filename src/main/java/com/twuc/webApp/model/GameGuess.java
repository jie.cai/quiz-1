package com.twuc.webApp.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public class GameGuess {
    @NotBlank
    @Length(max=4,min=4)
    private String answer;

    public String getAnswer() {
        return answer;
    }
}
