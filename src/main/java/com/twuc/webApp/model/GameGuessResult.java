package com.twuc.webApp.model;

public class GameGuessResult {
    private String hint;
    private Boolean correct;

    public GameGuessResult() {}

    public GameGuessResult(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
