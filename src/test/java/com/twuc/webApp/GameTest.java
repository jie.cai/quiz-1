package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Game;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GameTest {
    @Autowired
    MockMvc mockMvc;

    @Test // should_create_first_game_1
    void t1() throws Exception {
        mockMvc.perform(
                post("/api/games")
        )
                .andExpect(status().isCreated());
    }

    @Test // should_create_more_increment_round_game_2_3
    void t2() throws Exception {
        mockMvc.perform(
                post("/api/games")
        )
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/2"));
        mockMvc.perform(
                post("/api/games")
        )
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/3"));
    }

    @Test // should_get_a_round_game_by_id_1
    void t3() throws Exception {
        mockMvc.perform(
                get("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.answer").exists());
    }

    @Test // should_guess_game_1
    void t4() throws Exception {
        mockMvc.perform(
                patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{ \"answer\": \"1\" }")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").exists());
    }

    private Game getRound() throws Exception {
        MvcResult gameRequest = mockMvc.perform(
                get("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(gameRequest.getResponse().getContentAsString(), Game.class);
    }

    @Test // should_guess_right_answer
    void t5() throws Exception {
        Game game = getRound();
        String rightAnswer = game.getAnswer();

        mockMvc.perform(
                patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{ \"answer\": \""+rightAnswer+"\" }")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test // should_guess_error_answer
    void t6() throws Exception {
        Game game = getRound();
        String errorAnswer = new StringBuilder(game.getAnswer()).reverse().toString();

        mockMvc.perform(
                patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{ \"answer\": \""+errorAnswer+"\" }")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(false));
    }

    @Test // should_guess_error_answer_with_0A4B
    void t7() throws Exception {
        Game game = getRound();
        String errorAnswer = new StringBuilder(game.getAnswer()).reverse().toString();

        mockMvc.perform(
                patch("/api/games/1")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("{ \"answer\": \""+errorAnswer+"\" }")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(false))
                .andExpect(jsonPath("$.hint").value("0A4B"));
    }

    @Test
    void should_not_get_game_by_not_found_id() throws Exception {
        mockMvc.perform(
                get("/api/games/233")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isNotFound());
    }
}
